import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/services/generic-service';
import { Product } from 'src/app/models/product-model';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [GenericService,]
})
export class ProductListComponent implements OnInit {
  entity: Product = new Product();
  data: any[];
  cols: any[];
  countries: any[];
  displayDialog: boolean;

  constructor(private service: GenericService) { }

  ngOnInit() {
    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'name', header: 'Nombre', isSortable: true, isFilterable: true },
      { field: 'feature', header: 'Característica', isSortable: true },
      { field: 'country', header: 'País' }
    ];

    var self = this;
    self.service.getData().subscribe(data => self.data = data);
    self.service.getCountries().subscribe(data => {
      data.unshift({ label: '«Seleccione»', value: null });
      self.countries = data
    });
  }

  remove(index) {
    var self = this;
    self.data = self.data.filter((_, i) => i != index);
  }

  save() {
    var self = this;
    self.entity.id = self.guid();
    self.data.push(self.entity);
    self.entity = new Product();
    self.displayDialog = false;
  }

  showDialogAdd() {
    this.displayDialog = true;
  }

  guid() {
    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
  }
}
